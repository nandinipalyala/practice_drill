let got = require('../data')

//  5. Write a function called `nameWithA` which returns a array of names of all the people in `got` variable whose name includes `a` or `A`.

function nameWithA(got){
    let name = [];
    
    for(let house of got.houses){
        for(let peoples of house.people){
            if(peoples.name.includes("a") || peoples.name.includes("A")){
                name.push(peoples.name)
            }
        }
    }
    return name;
}
console.log(nameWithA(got));