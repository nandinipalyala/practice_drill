let got = require('../data')

// 7. Write a function called `surnameWithA` which returns a array of names of all the people in `got` variable whoes surname is starting with `A`(capital a).

function surnameWithA(got){
    let surname = [];

    for(let house of got.houses){
        for(let peoples of house.people){
            let splitName = peoples.name.split(' ')
            let lastName = splitName[splitName.length-1]
            if(lastName.charAt(0) === "A"){
                surname.push(peoples.name)
            }
        }
    }
    return surname;
}

console.log(surnameWithA(got));