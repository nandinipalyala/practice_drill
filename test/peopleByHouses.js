let got = require('../data')

// 2. Write a function called `peopleByHouses` which counts the total number of people in different houses in the `got` variable defined in `data.js` file.

function peopleByHouses(got){
    let counthouses = [];
    
    for(let house of got.houses){
        let count  = 0;
    for(let peoples of house.people){
        count ++;
    }
    counthouses[house.name] = count;
    }
    return counthouses;
}
console.log(peopleByHouses(got));