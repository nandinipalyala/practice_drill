let got = require('../data')

//  4. Write a function called `nameWithS` which returns a array of names of all the people in `got` variable whose name includes `s` or `S`.

function nameWithS(got){
    let name = [];
    
    for(let house of got.houses){
        for(let peoples of house.people){
            if(peoples.name.includes("s") || peoples.name.includes("S")){
                name.push(peoples.name)
            }
        }
    }
    return name;
}
console.log(nameWithS(got));