let got = require('../data')

//- 1. Write a function called `countAllPeople` which counts the total number of people in `got` variable defined in `data.js` file.
function countAllPeople() {
    let count = 0;
    
    for (let house of got.houses) {
       for (let person of house.people) {
         count++;
       }
    }
    return count;
    }
    console.log(countAllPeople())