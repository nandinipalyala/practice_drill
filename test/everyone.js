let got = require('../data')

// 3. Write a function called `everyone` which returns a array of names of all the people in `got` variable.
function everyone(got){
    let count = [];
    
    for(let house of got.houses){
        for(let peoples of house.people){
          count.push(peoples.name)
        }
    }
    return count;
}

console.log(everyone(got));