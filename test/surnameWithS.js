let got = require('../data')

// 6. Write a function called `surnameWithS` which returns a array of names of all the people in `got` variable whoes surname is starting with `S`(capital s).

function surnameWithS(got){
    let surname = [];
    
    for(let house of got.houses){
        for(let peoples of house.people){
            let splitName = peoples.name.split(' ');
            let lastName = splitName[splitName.length - 1];
            if (lastName.charAt(0) === 'S') {
                surname.push(peoples.name);
            }
        }
    }
    return surname;
}

console.log(surnameWithS(got));