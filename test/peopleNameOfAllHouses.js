let got = require('../data')

// 8. Write a function called `peopleNameOfAllHouses` which returns an object with the key of the name of house and value will be all the people in the house in an array.

function peopleNameOfAllHouses(got) {
    const houses = {};
  
    for (let house of got.houses) {
      const people = [];
  
      for (let peoples of house.people) {
        people.push(peoples.name);
      }
  
      houses[house.name] = people;
    }
  
    return houses;
  }

console.log(peopleNameOfAllHouses(got));